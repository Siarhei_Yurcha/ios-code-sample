import Foundation

public extension Data {
    enum Firebase {
        public enum Gateway {}
        public enum RemoteUseCases {
            public enum Tournament {}
            public enum Payment {}
            public enum Game {
                public enum Quiz {}
            }
        }
    }
}
