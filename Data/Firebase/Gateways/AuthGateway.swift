import Foundation
import Combine
import Firebase
import FirebaseFirestoreSwift
import FirebaseAuth
import Domain
import Utils

public extension Data.Firebase.Gateway {
    class AuthGateway: Domain.AuthGateway {

        public init() {}

        // MARK: - Inner types

        private enum LoginError: Error {
            case unknown
        }

        // MARK: - AuthGateway

        public func observeAuthState() -> AnyPublisher<Domain.AuthState, Error> {
            return PassthroughSubject<Domain.AuthState, Error>()
                .makeAutoConnectable { subject in
                    let listener: (FirebaseAuth.Auth, FirebaseAuth.User?) -> Void = { (_, user) in
                        guard let user = user else {
                            subject.send(Domain.AuthState())
                            return
                        }

                        user.getIDToken { token, _ in
                            subject.send(Domain.AuthState(accessToken: token,
                                                               currentUser: Domain.User(userId: user.uid)))
                        }
                    }
                    let authStateChangeListener = Auth.auth().addStateDidChangeListener(listener)
                    let tokenChangeListener = Auth.auth().addIDTokenDidChangeListener(listener)

                    return AnyCancellable {
                        Auth.auth().removeStateDidChangeListener(authStateChangeListener)
                        Auth.auth().removeIDTokenDidChangeListener(tokenChangeListener)
                    }
                }
        }

        public func login(with username: String, _ password: String) -> Future<Domain.AuthState, Error> {
            return .init { promise in
                Auth.auth().signIn(withEmail: username, password: password) { authResult, error in
                    guard let authResult = authResult, error == nil else {
                        promise(.failure(LoginError.unknown))
                        return
                    }

                    authResult.user.getIDToken { token, error in
                        guard error == nil else {
                            promise(.failure(LoginError.unknown))
                            return
                        }
                        let authState = Domain.AuthState(accessToken: token, currentUser: Domain.User(userId: authResult.user.uid))
                        promise(.success(authState))
                    }
                }
            }
        }
    }
}
