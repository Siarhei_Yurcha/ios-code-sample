import Foundation
import Combine
import Firebase
import FirebaseFirestoreSwift
import Domain
import Utils

public extension Data.Firebase.Gateway {

    enum RegistrationGatewayError: Error {
        case unableToBuildQuery
    }

    final class RegistrationGateway: Domain.RegistrationGateway {
        public init() {}

        public func observePage(filter: RegistrationListFilter,
                                pageSize: Int,
                                pageIndex: PageIndex) -> AnyPublisher<Page<Registration>.Data, Error> {

            func buildQuery() -> Query? {
                var query: Query
                if let tournamentId = filter.tournamentId {
                    query = Firestore.firestore()
                        .collection("tournaments")
                        .document(tournamentId)
                        .collection("tournament_registrations")
                        .limit(to: pageSize)
                } else {
                    query = Firestore.firestore()
                        .collectionGroup("tournament_registrations")
                        .limit(to: pageSize)
                }

                if let state = filter.state {
                    query = query.whereField("state", isEqualTo: state.rawValue)
                }

                if let userId = filter.userId {
                    query = query.whereField(FieldPath(["player", "id"]), isEqualTo: userId)
                }

                if case let PageIndex.byToken(pageToken) = pageIndex {
                    guard let lastDocumentSnapshot = pageToken as? DocumentSnapshot else {
                        return nil
                    }

                    query = query.start(afterDocument: lastDocumentSnapshot)
                }

                return query
            }

            guard let query = buildQuery() else {
                return Fail(error: RegistrationGatewayError.unableToBuildQuery).eraseToAnyPublisher()
            }

            return PassthroughSubject<Page<Registration>.Data, Error>()
                .makeAutoConnectable { subject in
                    let listenerRegistration = query.addSnapshotListener { querySnapshot, _ in
                        guard let documents = querySnapshot?.documents else { return }
                        let registrations = documents.compactMap { try? $0.data(as: Registration.self) }
                        let nextPageToken = documents.count == pageSize ? documents.last : nil
                        subject.send((elements: registrations, nextPageToken: nextPageToken))
                    }
                    return AnyCancellable(listenerRegistration.remove)
                }
        }

        public func observeTournamentRegistrationsInfo(tournamentId: String) -> AnyPublisher<RegistrationsInfo, Error> {
            let registrationsInfoRef = Firestore.firestore()
                .collection("tournaments")
                .document(tournamentId)
                .collection("registrationsInfo")

            return PassthroughSubject<RegistrationsInfo, Error>()
                .makeAutoConnectable { subject in
                    let listenerRegistration = registrationsInfoRef.addSnapshotListener { snapshot, error in
                        guard error == nil else { return }
                        guard let documents = snapshot?.documents else {
                            subject.send(RegistrationsInfo(reserved: 0, confirmed: 0, cancelled: 0, overdue: 0, finished: 0))
                            return
                        }
                        let infos = documents.compactMap { try? $0.data(as: RegistrationsInfo.self) }
                        let initialRegInfo = RegistrationsInfo(reserved: 0, confirmed: 0, cancelled: 0, overdue: 0, finished: 0)
                        let aggregatedInfos = infos.reduce(initialRegInfo) { aggregate, current in
                            return RegistrationsInfo(reserved: aggregate.reserved + current.reserved,
                                                     confirmed: aggregate.confirmed + current.confirmed,
                                                     cancelled: aggregate.cancelled + current.cancelled,
                                                     overdue: aggregate.overdue + current.overdue,
                                                     finished: aggregate.finished + current.finished)
                        }
                        subject.send(aggregatedInfos)
                    }
                    return AnyCancellable(listenerRegistration.remove)
                }
        }
    }
}
