import Foundation
import Combine
import Firebase
import FirebaseFirestoreSwift
import Domain
import Utils

public extension Data.Firebase.Gateway {

    enum TournamentGatewayError: Error {
        case unableToBuildQuery
        case unknown
    }

    final class TournamentGateway: Domain.TournamentGateway {

        public init() {}

        public func observePage(filter: TournamentListFilter,
                                pageSize: Int,
                                pageIndex: PageIndex) -> AnyPublisher<Page<TournamentEntity>.Data, Error> {

            func buildQuery() -> Query? {
                var query = Firestore.firestore().collection("tournaments").limit(to: pageSize)

                if let state = filter.state {
                    query = query.whereField("state", isEqualTo: state.rawValue)
                }

                if let startType = filter.startType {
                    query = query.whereField(FieldPath(["type", "startType"]), isEqualTo: startType.rawValue)
                }

                query = query.order(by: "startTime", descending: false)

                if case let PageIndex.byToken(pageToken) = pageIndex {
                    guard let lastDocumentSnapshot = pageToken as? DocumentSnapshot else {
                        return nil
                    }

                    query = query.start(afterDocument: lastDocumentSnapshot)
                }

                return query
            }

            guard let query = buildQuery() else {
                return Fail(error: TournamentGatewayError.unableToBuildQuery).eraseToAnyPublisher()
            }

            return PassthroughSubject<Page<TournamentEntity>.Data, Error>()
                .makeAutoConnectable { subject in
                    let listenerRegistration = query.addSnapshotListener { querySnapshot, _ in
                        guard let documents = querySnapshot?.documents else { return }
                        let tournaments = documents.compactMap { try? $0.data(as: TournamentEntity.self) }
                        let nextPageToken = tournaments.count == pageSize ? documents.last : nil
                        subject.send((elements: tournaments, nextPageToken: nextPageToken))
                    }
                    return AnyCancellable(listenerRegistration.remove)
                }
        }

        public func observeObject(by tournamentId: String) -> AnyPublisher<TournamentEntity, Error> {
            let tournamentRef = Firestore.firestore().collection("tournaments").document(tournamentId)

            return PassthroughSubject<TournamentEntity, Error>()
                .makeAutoConnectable { subject in
                    let listenerRegistration = tournamentRef.addSnapshotListener { snapshot, error in
                        guard let tournament = try? snapshot?.data(as: TournamentEntity.self), error == nil else { return }
                        subject.send(tournament)
                    }
                    return AnyCancellable(listenerRegistration.remove)
                }
        }
    }
}
