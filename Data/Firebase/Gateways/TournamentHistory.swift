import Foundation
import Combine
import Firebase
import FirebaseFirestoreSwift
import Domain
import Utils

public extension Data.Firebase.Gateway {

    enum TournamentHistoryGatewayError: Error {
        case unableToBuildQuery
        case unknown
    }

    final class TournamentHistoryGateway: ObservePaginatedListGateway {
        public init() {}

        public func observePage(filter: TournamentHistoryFilter,
                                pageSize: Int,
                                pageIndex: PageIndex) -> AnyPublisher<Core.Entity.Page<Tournament.Entity.Tournament>.Data, Error> {
            return observeTournamentIDsPage(filter: filter, pageSize: pageSize, pageIndex: pageIndex)
                .map { pageData -> AnyPublisher<Core.Entity.Page<Tournament.Entity.Tournament>.Data, Error> in
                    let initialPublisher = Just([TournamentEntity]()).setFailureType(to: Error.self).eraseToAnyPublisher()
                    return pageData.elements.reduce(initialPublisher) { combined, id -> AnyPublisher<[TournamentEntity], Error> in
                        combined
                            .combineLatest(TournamentGateway().observeObject(by: id))
                            .map { $0 + [$1] }
                            .eraseToAnyPublisher()
                    }
                    .map { (elements: $0, nextPageToken: pageData.nextPageToken) }
                    .eraseToAnyPublisher()
                }
                .switchToLatest()
                .map { pageData in
                    var elements = pageData.elements
                    switch filter.type {
                    case .active:
                        elements = elements.sorted { ($0.startTime ?? 0) < ($1.startTime ?? 0) }
                    case .finished:
                        elements = elements.sorted { ($0.startTime ?? 0) > ($1.startTime ?? 0) }
                    }

                    return (elements: elements, nextPageToken: pageData.nextPageToken)
                }
                .eraseToAnyPublisher()
        }

        // MARK: - Private

        private func observeTournamentIDsPage(filter: TournamentHistoryFilter,
                                              pageSize: Int,
                                              pageIndex: PageIndex) -> AnyPublisher<Page<String>.Data, Error> {
            func buildQuery() -> Query? {
                var query = Firestore.firestore()
                    .collectionGroup("tournament_registrations")
                    .limit(to: pageSize)

                query = query.whereField(FieldPath(["player", "id"]), isEqualTo: filter.userId)

                switch filter.type {
                case .active:
                    query = query.whereField("state", isEqualTo: Tournament.Entity.Registration.State.confirmed.rawValue)
                case .finished:
                    query = query.whereField("state", isEqualTo: Tournament.Entity.Registration.State.finished.rawValue)
                }

                query = query.order(by: "expirationTime", descending: true)

                if case let PageIndex.byToken(pageToken) = pageIndex {
                    guard let lastDocumentSnapshot = pageToken as? DocumentSnapshot else {
                        return nil
                    }

                    query = query.start(afterDocument: lastDocumentSnapshot)
                }

                return query
            }

            guard let query = buildQuery() else {
                return Fail(error: TournamentHistoryGatewayError.unableToBuildQuery).eraseToAnyPublisher()
            }

            return PassthroughSubject<Page<String>.Data, Error>()
                .makeAutoConnectable { subject in
                    let listenerRegistration = query.addSnapshotListener { querySnapshot, _ in
                        guard let documents = querySnapshot?.documents else {
                            subject.send((elements: [], nextPageToken: nil))
                            return
                        }
                        let tournamentIDs = documents.compactMap { $0.reference.parent.parent?.documentID }
                        let nextPageToken = documents.count == pageSize ? documents.last : nil
                        subject.send((elements: tournamentIDs, nextPageToken: nextPageToken))
                    }
                    return AnyCancellable(listenerRegistration.remove)
                }
        }

        private func loadTournament(by tournamentId: String) -> AnyPublisher<TournamentEntity, Error> {
            Future<TournamentEntity, Error> { promise in
                Firestore.firestore().collection("tournaments").document(tournamentId).getDocument { documentSnapshot, error in
                    guard let tournament = try? documentSnapshot?.data(as: TournamentEntity.self), error == nil else {
                        promise(.failure(TournamentHistoryGatewayError.unknown))
                        return
                    }

                    promise(.success(tournament))
                }
            }
            .eraseToAnyPublisher()
        }
    }
}
