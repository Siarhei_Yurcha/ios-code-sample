import Foundation
import Combine
import Alamofire
import FirebaseAuth

public extension Data.Firebase.RemoteUseCases {

    internal struct DomainError: Codable, Error {
        let domain: String
        let code: Int
        let message: String
    }

    struct Response<T: Codable>: Codable {
        let data: T?
        let error: DomainError?
    }

    internal enum HttpRemoteUseCaseError: Error {
        case unknown, userIsNotAuthorized
    }

    class HttpRemoteUseCase {
        let config: HttpRemoteUseCaseConfig

        public init(config: HttpRemoteUseCaseConfig) {
            self.config = config
        }

        func run<ResponseType: Codable, ErrorType: Error>(requestBuilder: @escaping (String) -> DataRequest,
                                                          internalErrorMapper: @escaping ((HttpRemoteUseCaseError) -> ErrorType),
                                                          domainErrorMapper: @escaping ((DomainError) -> ErrorType),
                                                          afErrorMapper: @escaping ((AFError) -> ErrorType)) -> Future<ResponseType, ErrorType> {
            return Future<ResponseType, ErrorType> { promise in
                guard let currentUser = Auth.auth().currentUser else {
                    promise(.failure(internalErrorMapper(.userIsNotAuthorized)))
                    return
                }

                currentUser.getIDToken(completion: { token, _ in

                    guard let token = token else {
                        promise(.failure(internalErrorMapper(.userIsNotAuthorized)))
                        return
                    }

                    requestBuilder(token).responseDecodable(of: Response<ResponseType>.self) { response in
                        print("FINISH API CALL:")
                        print(response.request?.url?.absoluteString ?? "")
                        if let data = response.data {
                            print(String(data: data, encoding: .utf8) ?? "")
                        }
                        switch response.result {
                        case .failure(let afError):
                            promise(.failure(afErrorMapper(afError)))

                        case.success(let response):
                            if let data = response.data {
                                promise(.success(data))
                            } else if let domainError = response.error {
                                promise(.failure(domainErrorMapper(domainError)))
                            } else {
                                promise(.failure(internalErrorMapper(.unknown)))
                            }
                        }
                    }
                })
            }
        }

        func run<ResponseType: Codable>(requestBuilder: @escaping (String) -> DataRequest) -> Future<ResponseType, Error> {
            run(requestBuilder: requestBuilder,
                internalErrorMapper: { $0 },
                domainErrorMapper: { $0 },
                afErrorMapper: { $0 })
        }
    }
}

public typealias HttpRemoteUseCase = Data.Firebase.RemoteUseCases.HttpRemoteUseCase
