import Foundation

public extension Data.Firebase.RemoteUseCases {
    struct HttpRemoteUseCaseConfig {
        let url: URL

        var tournamentEndpointURL: URL { url.appendingPathComponent("/tournament") }
        var paymentEndpointURL: URL { url.appendingPathComponent("/payment") }
        var quizGameEndpointURL: URL { url.appendingPathComponent("/game/quiz_game") }

        public init(url: URL) {
            self.url = url
        }
    }
}

public typealias HttpRemoteUseCaseConfig = Data.Firebase.RemoteUseCases.HttpRemoteUseCaseConfig
