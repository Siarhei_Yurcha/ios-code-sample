import Foundation
import Domain
import Combine
import Alamofire

public extension Data.Firebase.RemoteUseCases.Tournament {

    final class CreateRegistration: HttpRemoteUseCase, Domain.Tournament.UseCase.CreateRegistration {

        public func run(tournamentId: String, userId: String) -> Future<Registration, Error> {
            run { [config] token in
                let url = config.tournamentEndpointURL.absoluteString + "/\(tournamentId)/reservation/\(userId)/create"
                return AF.request(url, method: .post, headers: [.authorization(bearerToken: token)])
            }
        }
    }

    final class ConfirmRegistration: HttpRemoteUseCase, Domain.Tournament.UseCase.ConfirmRegistration {

        public func run(tournamentId: String, registrationId: String, transactionId: String) -> Future<Registration, Error> {
            run { [config] token in
                let url = config.tournamentEndpointURL.appendingPathComponent("/\(tournamentId)/reservation/\(registrationId)/confirm")
                return AF.request(url,
                                  method: .post,
                                  parameters: ["transactionId": transactionId],
                                  encoder: URLEncodedFormParameterEncoder(destination: .queryString),
                                  headers: [.authorization(bearerToken: token)])
            }
        }
    }

    final class CancelRegistration: HttpRemoteUseCase, Domain.Tournament.UseCase.CancelRegistration {

        public func run(tournamentId: String, registrationId: String) -> Future<Registration, Error> {
            run { [config] token in
                let url = config.tournamentEndpointURL.absoluteString + "/\(tournamentId)/registration/\(registrationId)/cancel"
                return AF.request(url, method: .post, headers: [.authorization(bearerToken: token)])
            }
        }
    }
}
