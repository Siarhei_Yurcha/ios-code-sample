import Foundation

public extension Auth.Entity {

    struct AuthState {
        public let accessToken: String?
        public let currentUser: User?

        public var isAuthorized: Bool {
            return accessToken != nil
        }

        public init(accessToken: String? = nil, currentUser: User? = nil) {
            self.accessToken = accessToken
            self.currentUser = currentUser
        }
    }
}

public typealias AuthState = Auth.Entity.AuthState
