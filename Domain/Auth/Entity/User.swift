import Foundation

public extension Auth.Entity {
    struct User {
        public let userId: String

        public init(userId: String) {
            self.userId = userId
        }
    }
}

public typealias User = Auth.Entity.User
