import Foundation
import Combine

public protocol AuthGateway {
    func observeAuthState() -> AnyPublisher<AuthState, Error>
    func login(with username: String, _ password: String) -> Future<AuthState, Error>
}

extension Auth.Gateway {
    typealias Gateway = AuthGateway
}
