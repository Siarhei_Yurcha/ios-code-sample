import Foundation
import Combine

public extension Auth.UseCase {
    struct BasicLogin {
        private let gateway: AuthGateway

        public init(gateway: AuthGateway) {
            self.gateway = gateway
        }

        public func run(userName: String, password: String) -> Future<AuthState, Error> {
            return self.gateway.login(with: userName, password)
        }
    }
}
