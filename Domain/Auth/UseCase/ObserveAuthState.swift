import Foundation
import Combine

public extension Auth.UseCase.Observe {
    struct AuthState {

        private let gateway: AuthGateway

        public init(gateway: AuthGateway) {
            self.gateway = gateway
        }

        public func run() -> AnyPublisher<Auth.Entity.AuthState, Error> {
            return self.gateway.observeAuthState()
        }
    }
}
