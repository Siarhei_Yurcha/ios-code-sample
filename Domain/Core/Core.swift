import Foundation

public enum Core {
    public enum Entity {}
    public enum UseCase {
        public enum Observe {}
    }
    public enum Gateway {}
}
