import Foundation
import Combine
import Utils

public extension Core.Entity {
    struct Page<Element> {

        public typealias Data = (elements: [Element], nextPageToken: Any?)

        // MARK: - Inner types

        public enum State {
            case loading, observing, error
        }

        // MARK: - Properties

        public let state: State
        public let elements: [Element]?
        let nextPageToken: Any?

        // MARK: - Init

        init(state: State, elements: [Element]? = nil, nextPageToken: Any? = nil) {
            self.state = state
            self.elements = elements
            self.nextPageToken = nextPageToken
        }
    }

    final class PageList<Element, Filter> {

        // MARK: - Properties

        @Published public var pages: [Page<Element>] = []
        @Published public var hasMore = true
        @Published public var isLoading = false
        public let pageSize: Int
        public let filter: Filter
        private var pageCancelables: [AnyCancellable] = []

        // MARK: - Init

        public init(pageSize: Int, filter: Filter) {
            self.pageSize = pageSize
            self.filter = filter
        }

        // MARK: - API

        func addPage(with pagePublisher: AnyPublisher<Page<Element>.Data, Error>) {
            pages.append(.init(state: .loading))
            isLoading = true
            let pageIndex = pages.count - 1

            pagePublisher
                .receive(on: DispatchQueue.main)
                .map { Page<Element>(state: .observing, elements: $0.elements, nextPageToken: $0.nextPageToken) }
                .catch { _ in Just(Page<Element>(state: .error)) }
                .sink { [weak self] page in
                    guard let self = self else { return }

                    self.pages[pageIndex] = page
                    self.hasMore = self.pages.isEmpty || self.pages.last?.nextPageToken != nil
                    self.isLoading = self.pages.last?.state == .loading
                }
                .store(in: &pageCancelables)
        }
    }
}

public typealias PageList = Core.Entity.PageList
public typealias Page = Core.Entity.Page
