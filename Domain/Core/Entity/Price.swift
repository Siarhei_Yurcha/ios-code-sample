import Foundation

public extension Core.Entity {

    struct Price: Codable {
        public let currency: String
        public let amount: Double

        public init(currency: String, amount: Double) {
            self.currency = currency
            self.amount = amount
        }
    }
}

public typealias Price = Core.Entity.Price
