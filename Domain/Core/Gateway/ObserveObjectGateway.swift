import Foundation
import Combine

public protocol ObserveObjectGateway {
    associatedtype Element

    func observeObject(by id: String) -> AnyPublisher<Element, Error>
}

public extension Core.Gateway {
    typealias ObserveObject = ObserveObjectGateway
}
