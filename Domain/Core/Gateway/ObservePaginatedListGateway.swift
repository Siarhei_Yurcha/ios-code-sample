import Foundation
import Combine

public protocol ObservePaginatedListGateway {
    associatedtype ListFilter
    associatedtype Element

    func observePage(filter: ListFilter,
                     pageSize: Int,
                     pageIndex: PageIndex) -> AnyPublisher<Page<Element>.Data, Error>
}

public extension Core.Gateway {
    typealias ObservePaginatedList = ObservePaginatedListGateway

    enum PageIndex {
        case first
        case byToken(Any)
    }
}

public typealias PageIndex = Core.Gateway.PageIndex
