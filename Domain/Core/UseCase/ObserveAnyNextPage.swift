import Foundation
import Combine

public extension Core.UseCase.Observe {
    struct NextPage<Entity, Filter> {
        private let observePage: (Filter, Int, PageIndex) -> AnyPublisher<Page<Entity>.Data, Error>

        // MARK: - Initializers

        public init<Gateway: ObservePaginatedListGateway>(gateway: Gateway)
        where Gateway.Element == Entity,
              Gateway.ListFilter == Filter {
            self.observePage = gateway.observePage
        }

        // MARK: - API

        public func run(list: PageList<Entity, Filter>) {
            guard !list.isLoading else { return }
            guard list.hasMore else { return }

            let pageIndex = list.pages.isEmpty ? PageIndex.first : PageIndex.byToken(list.pages.last!.nextPageToken!)
            let publisher = observePage(list.filter, list.pageSize, pageIndex)
            list.addPage(with: publisher)
        }
    }
}
