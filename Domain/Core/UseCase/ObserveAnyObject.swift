import Foundation
import Combine

public extension Core.UseCase.Observe {
    struct Object<Entity> {
        private let observeObject: (String) -> AnyPublisher<Entity, Error>

        // MARK: - Initializers

        public init<Gateway: ObserveObjectGateway>(gateway: Gateway)
        where Gateway.Element == Entity {
            self.observeObject = gateway.observeObject
        }

        public func run(objectId: String) -> AnyPublisher<Entity, Error> {
            return observeObject(objectId)
        }
    }
}
