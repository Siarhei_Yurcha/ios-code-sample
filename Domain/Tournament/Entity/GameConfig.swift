import Foundation

public extension Tournament.Entity {
    struct GameConfig: Codable {
        public let id: String
        public let name: String
        public let type: String
        //    public let config: Any
        public let createdAt: Int
        public let modifiedAt: Int
        public let ownerId: String
    }
}

public typealias GameConfig = Tournament.Entity.GameConfig
