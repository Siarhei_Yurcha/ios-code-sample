import Foundation

public extension Tournament.Entity {
    struct Player: Codable {
        public let name: String
        public let id: String
    }
}

public typealias Player = Tournament.Entity.Player
