import Foundation

public extension Tournament.Entity {

    struct Registration: Codable {
        public enum State: String, Codable {
            case reserved, confirmed, cancelled, overdue, finished
        }

        public let id: String
        public let expirationTime: Int
        public let player: Player
        public let state: State
        public let paymentId: String?
    }

    struct RegistrationsInfo: Codable {
        public let reserved: Int
        public let confirmed: Int
        public let cancelled: Int
        public let overdue: Int
        public let finished: Int
        public var active: Int {
            return finished + confirmed + reserved
        }

        public init(reserved: Int, confirmed: Int, cancelled: Int, overdue: Int, finished: Int) {
            self.reserved = reserved
            self.confirmed = confirmed
            self.cancelled = cancelled
            self.overdue = overdue
            self.finished = finished
        }
    }
}

public typealias Registration = Tournament.Entity.Registration
public typealias RegistrationsInfo = Tournament.Entity.RegistrationsInfo
