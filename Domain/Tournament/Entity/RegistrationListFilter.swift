import Foundation

public extension Tournament.Entity {

    struct RegistrationListFilter {
        public let state: Registration.State?
        public let userId: String?
        public let tournamentId: String?

        public init(tournamentId: String? = nil,
                    state: Registration.State? = nil,
                    userId: String? = nil) {
            self.tournamentId = tournamentId
            self.state = state
            self.userId = userId
        }
    }
}

public typealias RegistrationListFilter = Tournament.Entity.RegistrationListFilter
