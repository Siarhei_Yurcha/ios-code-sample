import Foundation

public extension Tournament.Entity {
    struct Tournament: Identifiable, Codable {
        public enum State: String, Codable {
            case draft
            case registrationOpen
            case registrationClosed
            case inPlay
            case sendingPayouts
            case finished
            case cancelling
            case cancelled
        }

        public let id: String
        public let type: TournamentType
        public let startTime: Int?
        public let state: State
        public let createdAt: Int
        public let modifiedAt: Int
        public let ownerId: String
        public let gameId: String
        public let results: [TournamentPayout]

        // MARK: - Computed properties
        public var startTimeDate: Date? {
            guard let startTime = self.startTime else { return nil }

            return Date(timeIntervalSince1970: TimeInterval(startTime) / 1000)
        }

        public var modifiedAtDate: Date {
            return Date(timeIntervalSince1970: TimeInterval(modifiedAt) / 1000)
        }
    }
}

public typealias TournamentEntity = Tournament.Entity.Tournament
