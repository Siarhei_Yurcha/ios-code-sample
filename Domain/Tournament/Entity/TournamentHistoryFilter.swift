import Foundation

public extension Tournament.Entity {
    struct TournamentHistoryFilter {
        public enum HistoryType: Hashable {
            case active, finished
        }
        public let userId: String
        public let type: HistoryType

        public init (userId: String, type: HistoryType) {
            self.type = type
            self.userId = userId
        }
    }
}

public typealias TournamentHistoryFilter = Tournament.Entity.TournamentHistoryFilter
