import Foundation

public extension Tournament.Entity {

    struct TournamentListFilter {
        public let startType: TournamentType.StartType?
        public let state: TournamentEntity.State?

        public init(startType: TournamentType.StartType? = nil,
                    state: TournamentEntity.State? = nil) {
            self.startType = startType
            self.state = state
        }
    }

}

public typealias TournamentListFilter = Tournament.Entity.TournamentListFilter
