import Foundation

public extension Tournament.Entity {

    struct TournamentPayout: Codable {
        public let place: Int
        public let playerId: String
        public let payoutAmount: Core.Entity.Price?
    }
}

public typealias TournamentPayout = Tournament.Entity.TournamentPayout
