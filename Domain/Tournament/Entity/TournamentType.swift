import Foundation

public extension Tournament.Entity {

    struct TournamentType: Codable {
        public enum StartType: String, Codable {
            case sng, scheduled
        }
        public let id: String
        public let createdAt: Int
        public let modifiedAt: Int
        public let name: String
        public let description: String
        public let gameConfig: GameConfig
        public let buyin: Price
        public let rake: Price
        public let maxPlayersCount: Int
        public let minPlayersCount: Int
        public let startType: StartType
        public let ownerId: String
        public let tags: [String]
        let payoutBuilder: String

        public var formattedDescription: String {
            return description.replacingOccurrences(of: "\\n", with: "\n")
        }
        public var payoutBuilderObject: PayoutBuilder? {
            guard let type = PayoutBuilderType(rawValue: payoutBuilder) else { return nil }
            switch type {
            case .powerLawFallOff:
                return PowerLawFallOffPayoutBuilder()
            }
        }
    }
}

public typealias TournamentType = Tournament.Entity.TournamentType
