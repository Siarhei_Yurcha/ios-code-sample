import Foundation
import Combine

public protocol RegistrationGateway: ObservePaginatedListGateway
where Element == Registration, ListFilter == RegistrationListFilter {
    func observeTournamentRegistrationsInfo(tournamentId: String) -> AnyPublisher<RegistrationsInfo, Error>
}

public extension Tournament.Gateway {
    typealias Registration = RegistrationGateway
}
