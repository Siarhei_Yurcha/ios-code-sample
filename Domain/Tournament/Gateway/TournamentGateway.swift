import Foundation
import Combine

public protocol TournamentGateway: ObservePaginatedListGateway, ObserveObjectGateway
where Element == TournamentEntity, ListFilter == TournamentListFilter {

}

public extension Tournament.Gateway {
    typealias Tournament = TournamentGateway
}
