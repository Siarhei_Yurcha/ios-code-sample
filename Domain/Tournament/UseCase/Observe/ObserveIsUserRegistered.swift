import Foundation
import Combine

public extension Tournament.UseCase.Observe {

    struct IsUserRegistered {
        private let userActiveRegistration: UserActiveRegistration

        public init<Gateway: RegistrationGateway>(gateway: Gateway) {
            self.userActiveRegistration = UserActiveRegistration(gateway: gateway)
        }

        public func run(tournamentId: String, userId: String) -> AnyPublisher<Bool, Error> {
            return userActiveRegistration.run(tournamentId: tournamentId, userId: userId)
                .map { $0 != nil }
                .eraseToAnyPublisher()
        }
    }
}
