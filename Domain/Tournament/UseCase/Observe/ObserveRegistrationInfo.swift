import Foundation
import Combine

public extension Tournament.UseCase.Observe {

    struct RegistrationInfo {
        private let _run: (String) -> AnyPublisher<RegistrationsInfo, Error>

        // MARK: - Initializers

        public init<Gateway: RegistrationGateway>(gateway: Gateway) {
            self._run = gateway.observeTournamentRegistrationsInfo
        }

        public func run(tournamentId: String) -> AnyPublisher<RegistrationsInfo, Error> {
            return _run(tournamentId)
        }
    }
}
