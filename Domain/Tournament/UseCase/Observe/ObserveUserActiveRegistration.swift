import Foundation
import Combine

public extension Tournament.UseCase.Observe {

    struct UserActiveRegistration {
        private let _run: (String, String) -> AnyPublisher<Registration?, Error>

        public init<Gateway: RegistrationGateway>(gateway: Gateway) {
            self._run = { tournamentId, userId in
                gateway
                    .observePage(filter: RegistrationListFilter(tournamentId: tournamentId, userId: userId),
                                 pageSize: .max,
                                 pageIndex: .first)
                    .map { $0.elements.first(where: { $0.state == .reserved || $0.state == .confirmed }) }
                    .eraseToAnyPublisher()
            }
        }

        public func run(tournamentId: String, userId: String) -> AnyPublisher<Registration?, Error> {
            return _run(tournamentId, userId)
        }
    }
}
