import Foundation
import Combine

// MARK: - Create

public protocol CreateTournamentRegistrationUseCase {
    func run(tournamentId: String, userId: String) -> Future<Registration, Error>
}

public extension Tournament.UseCase {
    typealias CreateRegistration = CreateTournamentRegistrationUseCase
}

// MARK: - Confirm

public protocol ConfirmTournamentRegistrationUseCase {
    func run(tournamentId: String, registrationId: String, transactionId: String) -> Future<Registration, Error>
}

public extension Tournament.UseCase {
    typealias ConfirmRegistration = ConfirmTournamentRegistrationUseCase
}

// MARK: - Register User

public extension Tournament.UseCase {

    enum RegisterUserError: Error {
        case unknown, insufficientFunds
    }

    struct RegisterUser {

        private struct Constants {
            static let numberOfRetries = 2
        }

        let createRegistrationUseCase: Tournament.UseCase.CreateRegistration
        let confirmRegistrationUseCase: Tournament.UseCase.ConfirmRegistration
        let submitTransactionUseCase: Payment.UseCase.SubmitPaymentTransaction
        let tournamentSystemIdInPaymentSystem: String

        public init(createRegistrationUseCase: Tournament.UseCase.CreateRegistration,
                    confirmRegistrationUseCase: Tournament.UseCase.ConfirmRegistration,
                    submitTransactionUseCase: Payment.UseCase.SubmitPaymentTransaction,
                    tournamentSystemIdInPaymentSystem: String) {
            self.createRegistrationUseCase = createRegistrationUseCase
            self.confirmRegistrationUseCase = confirmRegistrationUseCase
            self.submitTransactionUseCase = submitTransactionUseCase
            self.tournamentSystemIdInPaymentSystem = tournamentSystemIdInPaymentSystem
        }

        public func run(tournament: TournamentEntity, userId: String) -> AnyPublisher<Registration, RegisterUserError> {
            createRegistrationUseCase.run(tournamentId: tournament.id, userId: userId)
                .retry(Constants.numberOfRetries)
                .map {
                    purchase(registration: $0, in: tournament)
                        .retry(Constants.numberOfRetries)
                }
                .switchToLatest()
                .map {
                    confirmRegistrationUseCase.run(tournamentId: tournament.id,
                                                   registrationId: $0.registration.id,
                                                   transactionId: $0.transaction.id ?? "")
                        .retry(Constants.numberOfRetries)
                }
                .switchToLatest()
                .mapError { map(error: $0) }
                .eraseToAnyPublisher()
        }

        // MARK: - Private

        private func purchase(registration: Registration,
                              in tournament: TournamentEntity) -> AnyPublisher<(registration: Registration,
                                                                                transaction: PaymentTransaction), Error> {
            let paymentTransaction = PaymentTransaction(senderId: registration.player.id,
                                                        price: tournament.type.buyin,
                                                        receiverId: tournamentSystemIdInPaymentSystem,
                                                        purpose: "tournament/\(tournament.id)/registration/\(registration.id)")
            return submitTransactionUseCase.run(transaction: paymentTransaction)
                .map { (registration, $0) }
                .mapError { $0 as Error }
                .eraseToAnyPublisher()
        }

        private func map(error: Error) -> RegisterUserError {
            switch error {

            case let paymentError as Payment.UseCase.SubmitPaymentTransactionError where paymentError == .insufficientFunds:
                return .insufficientFunds

            default:
                return .unknown
            }
        }
    }
}

// MARK: - Cancel registration

public protocol CancelTournamentRegistrationUseCase {
    func run(tournamentId: String, registrationId: String) -> Future<Registration, Error>
}

public extension Tournament.UseCase {
    typealias CancelRegistration = CancelTournamentRegistrationUseCase
}
