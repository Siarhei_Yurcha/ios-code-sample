import Foundation
import Domain
import UIKit
import SwiftUI
import Data

protocol TournamentDetailsRouter: Router {
    func goToGameDetails(for tournament: TournamentEntity)
}

final class TournamentDetailsRouterImpl: TournamentDetailsRouter, ControllerRouter {
    let controller: UIViewController

    init(controller: UIViewController) {
        self.controller = controller
    }

    func goToGameDetails(for tournament: TournamentEntity) {
        let gameDetailsUseCases = QuizGameViewModel.UseCases(
            observeGame: .init(gateway: Data.Firebase.Gateway.QuizGameGateway()),
            observeQuestion: .init(gateway: Data.Firebase.Gateway.QuizGameQuestionGateway()),
            observeUserAnswers: .init(gateway: Data.Firebase.Gateway.QuizGameRegistrationGateway()),
            answerQuestion: Data.Firebase.RemoteUseCases.Game.Quiz.AnswerGameQuestion.init(config: Config.httpRemoteUseCaseConfig),
            observeAuthState: .init(gateway: Data.Firebase.Gateway.AuthGateway())
        )
        let gameDetailsModel = QuizGameViewModel(useCases: gameDetailsUseCases, gameId: tournament.gameId)
        let gameDetailsView = QuizGameView(viewModel: gameDetailsModel)
        let gameDetailsController = UIHostingController(rootView: gameDetailsView)
        gameDetailsModel.router = QuizGameRouterImpl(controller: gameDetailsController, closeHandler: {
            gameDetailsController.navigationController?.dismiss(animated: true)
        })
        let navController = UINavigationController(rootViewController: gameDetailsController)
        navController.modalPresentationStyle = .fullScreen

        present(navController, animated: true)
    }
}
