import SwiftUI

struct TournamentDetailsView: View {
    @ObservedObject var viewModel: TournamentDetailsViewModel
    @State var isPrizePoolStructureCollapsed = true
    @State var isTournamentInfoCollapsed = true

    init(viewModel: TournamentDetailsViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {
        VStack {
            ScrollView {
                LazyVStack {
                    buyinView()
                    TagsView(tags: viewModel.tags)
                    TournamentStateView(state: viewModel.state)

                    section(collapsed: $isPrizePoolStructureCollapsed,
                            items: viewModel.prizePoolStructure) {
                        section(title: L10n.Tournament.totalPrizePool.uppercased())
                        Text("\(viewModel.totalPrizePool.uppercased())").font(.headline)
                    }
                    .padding(.top, 40)

                    section(collapsed: $isTournamentInfoCollapsed,
                            items: [viewModel.tournamentInfo]) {
                        section(title: L10n.Tournament.detailedInfo.uppercased())
                    }
                }
                .animation(.spring())
            }
            button(for: viewModel.action)
        }
        .navigationTitle(viewModel.tournamentName)
        .onAppear { [weak viewModel] in viewModel?.handleOnAppear() }
        .onDisappear { [weak viewModel] in viewModel?.handleOnDisappear() }
    }

    // MARK: - Private

    @ViewBuilder private func buyinView() -> some View {
        HStack {
            Text("\(L10n.Tournament.buyin.capitalized):")
                .font(.subheadline)
                .bold()
                .opacity(0.5)
            Text(viewModel.buyin)
                .font(.subheadline)
            Spacer()
        }.padding(.leading, 16)
    }

    @ViewBuilder private func section<Title: View>(collapsed: Binding<Bool>,
                                                   items: [String],
                                                   @ViewBuilder title: @escaping () -> Title) -> some View {
        VStack(spacing: 0) {
            CollapsableSectionHeader(isCollapsed: collapsed) {
                title()
            }

            if !collapsed.wrappedValue {
                ForEach(items, id: \.self) { item in
                    HStack {
                        Text(item).font(.body).lineLimit(nil)
                        Spacer()
                    }.padding(.init(top: 5, leading: 40, bottom: 5, trailing: 0))
                }
                .transition(.move(edge: .top).combined(with: .opacity))
            }

            Color(Asset.Colors.General.listHeaderBackground.color)
                .frame(maxWidth: .infinity, minHeight: 1, maxHeight: 1)
        }
    }

    @ViewBuilder private func section(title: String) -> some View {
        Text("\(title): ")
            .font(.headline)
            .opacity(0.5)
            .padding(.leading, 20)
    }

    @ViewBuilder private func button(for action: TournamentDetailsViewModel.Action) -> some View {
        switch viewModel.action {
        case .none:
            EmptyView()

        case .register:
            actionButton(buttonStyle: PrimaryActionButtonStyle(),
                         title: L10n.Tournament.Action.register.uppercased())

        case .unregister:
            actionButton(buttonStyle: DestructiveActionButtonStyle(),
                         title: L10n.Tournament.Action.cancelRegistration.uppercased())

        case .openGame:
            actionButton(buttonStyle: PrimaryActionButtonStyle(),
                         title: L10n.Tournament.Action.openGame.uppercased())
        }
    }

    @ViewBuilder private func actionButton<T: ButtonStyle>(buttonStyle: T, title: String) -> some View {
        Button(action: { [weak viewModel] in viewModel?.processAction() },
               label: { Text(title).font(.headline) })
        .buttonStyle(buttonStyle)
        .padding()
    }
}
