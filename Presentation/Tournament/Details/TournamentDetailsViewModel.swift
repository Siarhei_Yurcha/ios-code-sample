import Foundation
import Domain
import Combine

final class TournamentDetailsViewModel: ViewModel, ObservableObject {

    // MARK: - Use cases

    struct UseCases {
        let observeTournament: Core.UseCase.Observe.Object<TournamentEntity>
        let observeTournamentRegistrationsInfo: Tournament.UseCase.Observe.RegistrationInfo
        let observeUserActiveRegistration: Tournament.UseCase.Observe.UserActiveRegistration
        let observeAuthState: Auth.UseCase.Observe.AuthState
        let registerUser: Tournament.UseCase.RegisterUser
        let unregisterUser: Tournament.UseCase.CancelRegistration
    }

    // MARK: - Constants

    private struct Constants {
        static let priceFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.maximumFractionDigits = 2
            return formatter
        }()
    }

    // MARK: - Inner types

    enum State {
        case undefined
        case registration(startTime: TimeLabelViewModel, registrationsInfo: String)
        case inProgress(registrationsInfo: String)
        case cancelled(cancellationTime: TimeLabelViewModel)
        case sendingPayouts(place: String, receivedPayout: String)
        case finished(finishTime: TimeLabelViewModel, place: String, receivedPayout: String)
    }

    enum Action {
        case none
        case register
        case unregister
        case openGame
    }

    // MARK: - Properties

    @Published private(set) var tournamentName: String = ""
    @Published private(set) var tags: [String] = []
    @Published private(set) var buyin: String = ""
    @Published private(set) var state = State.undefined
    @Published private(set) var action = Action.none
    @Published private(set) var totalPrizePool: String = ""
    @Published private(set) var prizePoolStructure: [String] = []
    @Published private(set) var tournamentInfo: String = ""

    @Published private var tournament: TournamentEntity
    @Published private var registrationsInfo: RegistrationsInfo?
    @Published private var authState = Domain.AuthState()
    @Published private var userRegistration: Registration?

    private let useCases: UseCases
    var router: TournamentDetailsRouter?

    // MARK: - Init

    init(useCases: UseCases, tournament: TournamentEntity) {
        self.tournament = tournament
        self.useCases = useCases

        super.init()

        setupDataBindings()
    }

    // MARK: - Data loading

    override func startDataObserving() {
        super.startDataObserving()

        useCases.observeAuthState.run()
            .catch { _ in Empty().eraseToAnyPublisher() }
            .sink { [weak self] in self?.authState = $0 }
            .store(in: &observingDataCancellables)

        useCases.observeTournament.run(objectId: tournament.id)
            .catch { _ in Empty().eraseToAnyPublisher() }
            .sink { [weak self] in self?.tournament = $0 }
            .store(in: &observingDataCancellables)

        $authState
            .map { [weak self] authState -> AnyPublisher<Registration?, Never> in
                guard let self = self, let userId = authState.currentUser?.userId else {
                    return Just(nil).eraseToAnyPublisher()
                }

                return self.useCases.observeUserActiveRegistration
                    .run(tournamentId: self.tournament.id, userId: userId)
                    .catch { _ in Just(nil) }
                    .eraseToAnyPublisher()
            }
            .switchToLatest()
            .sink { [weak self] in self?.userRegistration = $0 }
            .store(in: &observingDataCancellables)

        useCases.observeTournamentRegistrationsInfo.run(tournamentId: tournament.id)
            .catch { _ in Empty().eraseToAnyPublisher() }
            .sink { [weak self] in self?.registrationsInfo = $0 }
            .store(in: &observingDataCancellables)
    }

    // MARK: - Data bindings

    private func setupDataBindings() {
        $tournament.map(\.type.name).assign(to: &$tournamentName)
        $tournament.map(\.type.tags).assign(to: &$tags)
        $tournament.map(\.type.tags).assign(to: &$tags)
        $tournament.map(\.type.formattedDescription).assign(to: &$tournamentInfo)
        $tournament.map {
            "\(Constants.priceFormatter.string(from: NSNumber(value: $0.type.buyin.amount)) ?? "") \($0.type.buyin.currency)"
        }.assign(to: &$buyin)

        bindTournamentDataToTotalPrizePool()
        bindTournamentDataToPrizeStructure()
        bindTournamentDataToAction()
        bindTournamentDataToState()
    }

    private func bindTournamentDataToPrizeStructure() {
        $tournament.combineLatest($registrationsInfo) { (tournament, registrationsInfo) -> [String] in
            guard let payoutBuilder = tournament.type.payoutBuilderObject else { return [] }

            let registrationsCount = registrationsInfo?.active ?? 0
            let payouts = payoutBuilder.buildPrizeStructure(for: tournament, registrationsCount: registrationsCount).payouts

            return payouts.enumerated().map { index, payout in
                let formattedPayout = Constants.priceFormatter.string(from: NSNumber(value: payout)) ?? ""
                return "\(L10n.Tournament.place.capitalized) \(index + 1):    \(formattedPayout) \(tournament.type.buyin.currency)"
            }
        }.assign(to: &$prizePoolStructure)
    }

    private func bindTournamentDataToTotalPrizePool() {
        $tournament.combineLatest($registrationsInfo) { (tournament, registrationsInfo) -> String in
            guard let payoutBuilder = tournament.type.payoutBuilderObject else { return "" }

            let registrationsCount = registrationsInfo?.active ?? 0
            let totalPrizePool = payoutBuilder.buildPrizeStructure(for: tournament, registrationsCount: registrationsCount).totalPrizePool
            let formattedTotalPrizePool = Constants.priceFormatter.string(from: NSNumber(value: totalPrizePool)) ?? ""
            return "\(formattedTotalPrizePool) \(tournament.type.buyin.currency)"
        }.assign(to: &$totalPrizePool)
    }

    private func bindTournamentDataToAction() {
        $tournament.combineLatest($userRegistration) { (tournament, userRegistration) -> Action in
            let isRegisteredInTournament = userRegistration != nil && userRegistration?.state == .confirmed

            switch (tournament.state, isRegisteredInTournament) {
            case (.registrationOpen, true):
                return .unregister
            case (.registrationOpen, false):
                return .register
            case (.inPlay, true):
                return .openGame
            default:
                return .none
            }
        }.assign(to: &$action)
    }

    private func bindTournamentDataToState() {
        let timer = Timer.publish(every: 1, on: .main, in: .default)
            .autoconnect()
            .eraseToAnyPublisher()

        $tournament.combineLatest($registrationsInfo, $authState) { [weak self] (tournament, registrationsInfo, authState) -> State in

            guard let self = self else { return .undefined }

            switch tournament.state {

            case .draft:
                return .undefined

            case .registrationOpen, .registrationClosed:
                guard let startTime = tournament.startTimeDate else { return .undefined }
                let startTimeModel = TimeLabelViewModel(mode: .deadline(eventPassedPlaseholder: "...\(L10n.Common.starting)"),
                                                        time: startTime,
                                                        updateTimer: timer)
                let regsInfo = self.confirmedRegistrations(for: tournament, registrationsInfo)

                return .registration(startTime: startTimeModel, registrationsInfo: regsInfo)

            case .inPlay:
                let regsInfo = self.confirmedRegistrations(for: tournament, registrationsInfo)
                return .inProgress(registrationsInfo: regsInfo)

            case .cancelled, .cancelling:
                let timeModel = TimeLabelViewModel(mode: .info, time: tournament.modifiedAtDate)
                return .cancelled(cancellationTime: timeModel)

            case .sendingPayouts:
                let (place, receivedPayout) = self.results(of: authState.currentUser?.userId, in: tournament)
                return .sendingPayouts(place: place, receivedPayout: receivedPayout)

            case .finished:
                let finishTime = TimeLabelViewModel(mode: .info, time: tournament.modifiedAtDate)
                let (place, receivedPayout) = self.results(of: authState.currentUser?.userId, in: tournament)

                return .finished(finishTime: finishTime, place: place, receivedPayout: receivedPayout)
            }
        }.assign(to: &$state)
    }

    // MARK: - Private API

    private func results(of userId: String?, in tournament: TournamentEntity) -> (place: String, receivedPayout: String) {
        var place = "..."
        var receivedPayout = "..."

        if let userResult = tournament.results.first(where: { $0.playerId == userId }) {
            place = "\(userResult.place)"

            if let formattedPaymentAmount = Constants.priceFormatter.string(from: NSNumber(value: userResult.payoutAmount?.amount ?? 0)) {
                receivedPayout = "\(formattedPaymentAmount) \(tournament.type.buyin.currency)"
            }
        }

        return (place, receivedPayout)
    }

    private func confirmedRegistrations(for tournament: TournamentEntity, _ registrationsInfo: RegistrationsInfo?) -> String {
        return registrationsInfo != nil ? "\(registrationsInfo!.active)" : "...\(L10n.Common.loading)"
    }

    // MARK: - Input API

    func processAction() {
        guard let userId = authState.currentUser?.userId else { return }

        switch action {
        case .none:
            break

        case .openGame:
            router?.goToGameDetails(for: tournament)

        case .register:
            router?.show(status: L10n.Tournament.Action.register.capitalized, interactionEnabled: false)
            useCases.registerUser.run(tournament: tournament,
                                      userId: userId)
                .sink { [router] completion in
                    if case let .failure(error) = completion {
                        switch error {
                        case .insufficientFunds:
                            router?.showFailed(message: L10n.Tournament.Error.insufficientFunds)

                        case .unknown:
                            router?.showFailed(message: L10n.Tournament.Error.unknown)
                        }
                    }
                } receiveValue: { [weak self] registration in
                    self?.userRegistration = registration
                    self?.router?.showSucceed(message: nil)
                }
                .store(in: &longTermCancellables)

        case .unregister:
            guard let registrationId = userRegistration?.id else { return }

            router?.show(status: L10n.Tournament.Action.cancelRegistration.capitalized, interactionEnabled: false)

            useCases.unregisterUser.run(tournamentId: tournament.id,
                                        registrationId: registrationId)
                .sink { [router] completion in
                    if case .failure(_) = completion {
                        router?.showFailed(message: L10n.Tournament.Error.unknown)
                    }
                } receiveValue: { [weak self] _ in
                    self?.userRegistration = nil
                    self?.router?.showSucceed(message: nil)
                }
                .store(in: &longTermCancellables)
        }
    }
}
