import SwiftUI

struct TournamentStateView: View {
    private let state: TournamentDetailsViewModel.State

    init(state: TournamentDetailsViewModel.State) {
        self.state = state
    }

    @ViewBuilder
    var body: some View {
        switch state {

        case .undefined:
            stateContainer {
                Text(L10n.Tournament.State.undefined)
                    .font(.headline)
                    .multilineTextAlignment(.center)
            }
            .frame(height: 160)

        case .registration(let startTime, let registrationsInfo):
            detailsView(title: L10n.Tournament.State.registration,
                        textAttributes: [(L10n.Tournament.participants, registrationsInfo)],
                        timeAttributes: [(L10n.Tournament.start, startTime)])

        case .inProgress(let registrationsInfo):
            detailsView(title: L10n.Tournament.State.inProgress,
                        textAttributes: [(L10n.Tournament.participants, registrationsInfo)])

        case .cancelled(let cancellationTime):
            detailsView(title: L10n.Tournament.State.cancelled,
                        timeAttributes: [(L10n.Tournament.cancelled, cancellationTime)])

        case .sendingPayouts(let place, let receivedPayout):
            detailsView(title: L10n.Tournament.State.sendingPayouts,
                        textAttributes: [(L10n.Tournament.yourPlace, place),
                                         (L10n.Tournament.yourPayout, receivedPayout)])

        case .finished(let finishTime, let place, let receivedPayout):
            detailsView(title: L10n.Tournament.State.finished,
                        textAttributes: [(L10n.Tournament.yourPlace, place),
                                         (L10n.Tournament.yourPayout, receivedPayout)],
                        timeAttributes: [(title: L10n.Tournament.finished, value: finishTime)])
        }
    }

    // MARK: - Private

    @ViewBuilder private func stateContainer<Content: View>(_ content: () -> Content) -> some View {
        ZStack {
            Color(Asset.Colors.General.listHeaderBackground.color)
            content()
        }
    }

    @ViewBuilder private func detailsView(title: String,
                                          textAttributes: [(title: String, value: String)] = [],
                                          timeAttributes: [(title: String, value: TimeLabelViewModel)] = []) -> some View {
        stateContainer {
            VStack(alignment: .center) {
                Text(title).font(.title).padding()

                HStack {
                    VStack(alignment: .leading, spacing: 5) {
                        let titles = timeAttributes.map { $0.title } + textAttributes.map { $0.title }
                        ForEach(titles, id: \.self) { title in
                            Text("\(title):  ")
                                .font(.body)
                                .opacity(0.5)
                        }
                    }
                    VStack(alignment: .leading, spacing: 5) {
                        ForEach(timeAttributes, id: \.title) { attribute in
                            TimeLabelView(viewModel: attribute.value)
                                .font(.body)
                                .cornerRadius(4.0)
                        }
                        ForEach(textAttributes, id: \.title) { attribute in
                            Text(attribute.value).font(.body)
                        }
                    }
                }
                .padding()
            }
        }
    }
}

struct TournamentStateView_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            LazyVStack {
                TournamentStateView(state: .undefined)
            }
        }
        .preferredColorScheme(.dark)
    }
}
