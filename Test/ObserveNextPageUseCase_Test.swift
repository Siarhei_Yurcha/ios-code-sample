import Foundation
import Quick
import Nimble
import Combine
@testable import Domain

// swiftlint:disable function_body_length
struct TestTournamentGateway: TournamentGateway {
    struct NotImplemented: Error {}

    func observeTournament(tournamentId: String) -> AnyPublisher<Tournament, Error> {
        return Fail(error: NotImplemented()).eraseToAnyPublisher()
    }

    public func observePage(filter: TournamentListFilter, pageSize: Int, pageIndex: PageIndex) -> AnyPublisher<Page<Tournament>.Data, Error> {
        let today = Int(Date().timeIntervalSince1970) * 1000
        let tournament = Tournament(id: "test 1",
                                    type: TournamentType(id: "test 1",
                                                         createdAt: today,
                                                         modifiedAt: today,
                                                         name: "Sunday storm",
                                                         gameConfig: GameConfig(id: "test 1",
                                                                                name: "test 1",
                                                                                type: "test 1",
                                                                                createdAt: today,
                                                                                modifiedAt: today,
                                                                                ownerId: "test 1"),
                                                         buyin: Price(currency: "test 1", amount: 12),
                                                         rake: Price(currency: "test 1", amount: 1),
                                                         maxPlayersCount: 2,
                                                         minPlayersCount: 2,
                                                         startType: .scheduled,
                                                         ownerId: "test 1", tags: ["Math", "Genius"]),
                                    startTime: nil,
                                    state: .registrationOpen,
                                    createdAt: today,
                                    modifiedAt: today,
                                    ownerId: "test 1",
                                    gameId: "test 1",
                                    results: [])
        return Just((elements: Array(1...pageSize).map { _ in tournament }, nextPageToken: ""))
            .setFailureType(to: Error.self).eraseToAnyPublisher()
    }
}

class ObserveNextPageUseCaseSpec: QuickSpec {
    override func spec() {
        describe("Updating of the List.pages property upon page loading") {
            var list: PageList<Tournament, TournamentListFilter>!
            var useCase: ObserveNextPageUseCase<Tournament, TournamentListFilter>!
            var cancalable: AnyCancellable?

            beforeEach {
                list = PageList<Tournament, TournamentListFilter>(pageSize: 2, filter: TournamentListFilter())
                useCase = ObserveNextPageUseCase(gateway: TestTournamentGateway())
                cancalable = nil
            }

            context("No pages were loaded") {
                it("List should be empty") {
                    expect(list.pages.count).to(equal(0))
                }
            }

            context("Load fisrt page in the list") {
                it("Fisrt page SHOULD be added to the list with state .loading") {
                    waitUntil { done in
                        var callIndex = -1
                        cancalable = list.$pages.sink(receiveValue: { pages in
                            callIndex += 1
                            guard callIndex == 1 else { return }
                            expect(pages.count).to(equal(1))
                            expect(pages.first?.state).to(equal(.loading))
                            expect(pages.first?.nextPageToken).to(beNil())
                            expect(pages.first?.elements).to(beNil())
                            done()
                        })
                        useCase.run(list: list)
                    }
                }

                it("Fisrt page SHOULD be updated with loaded data") {
                    waitUntil { done in
                        var callIndex = -1
                        cancalable = list.$pages.sink(receiveValue: { pages in
                            callIndex += 1
                            guard callIndex == 2 else { return }
                            expect(pages.count).to(equal(1))
                            expect(pages.first?.nextPageToken as? String).to(equal(""))
                            expect(pages.first?.elements?.count).to(equal(2))
                            done()
                        })
                        useCase.run(list: list)
                    }
                }

                it("Should not update list once first page was loaded") {
                    waitUntil { done in
                        var callIndex = -1
                        cancalable = list.$pages.sink(receiveValue: { _ in
                            callIndex += 1
                            expect(callIndex).to(beLessThan(3))
                        })
                        useCase.run(list: list)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
                            done()
                        }
                    }
                }
            }

            context("Load second page in the list") {
                it("Second page SHOULD be added to the list with state .loading") {
                    waitUntil { done in
                        var callIndex = -1
                        cancalable = list.$pages.sink(receiveValue: { pages in
                            callIndex += 1
                            guard callIndex == 3 else { return }
                            expect(pages.count).to(equal(2))
                            expect(pages.last?.state).to(equal(.loading))
                            expect(pages.last?.nextPageToken).to(beNil())
                            expect(pages.last?.elements).to(beNil())
                            done()
                        })
                        useCase.run(list: list)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            useCase.run(list: list)
                        }
                    }
                }

                it("Second page SHOULD be updated with loaded data") {
                    waitUntil { done in
                        var callIndex = -1
                        cancalable = list.$pages.sink(receiveValue: { pages in
                            callIndex += 1
                            guard callIndex == 4 else { return }
                            expect(pages.count).to(equal(2))
                            expect(pages.last?.nextPageToken as? String).to(equal(""))
                            expect(pages.last?.elements?.count).to(equal(2))
                            done()
                        })
                        useCase.run(list: list)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                            useCase.run(list: list)
                        }
                    }
                }
            }
        }
    }
}
// swiftlint:enable function_body_length
