# What is this code about?

It is implementation of the tournament details page. 

This page has the following features:

* display tournament info (number of participants, prize pool, state and etc.)
* observe tournament data to display relevant information (e.g. when the new user registered in the tournament this info is automatically reflected on our side without any additional actions)
* depending on state user can perform the following actions:
    * register in the tournament
    * cancel registration
    * open game

# File structure

This project was built according to **Clean Architecture** principles.

## Domain

**Domain** folder contains *Entities* and *Use Cases* and that is the core of the app. Besides that this layer includes gateways protocols and it is done to get rid of dependencies on concrete DB and other services (actually it is the implementation of *Dependency inversion principle*).

Also, there is one more important concept - **remote use case**. In any client-server application, most of the use cases are implemented on the server-side, and in most apps, I've seen, we implement something like RegistrationManager (RegistrationService) with all registration methods that are exposed by our Backend API. But in the end, each method is just a Use Case that was implemented on the server and not in the app and we can access it via some protocol (usually http(s)). 

So **Remote Use Case** idea is as simple as it sounds - any use case that is implemented on the backend side should be represented by Protocol in Domain layer. And concrete implementation should be provided by the *Data* layer since the *Data* layer knows what protocol to use and other low-level data.

## Data

This folder contains the concrete implementation of gateways and remote use cases from the *Domain* layer

## Presentation

Presentation layer is represented by the MVVM pattern. So here we have **View Model** and View. For this project View was built in SwiftUI but it can be easily implemented in UIKit without any side effects to View Model or especially Domain/Data layers.

Additionally, this layer contains a *Router* object. This object is responsible for how a certain view module is shown on the screen. This object was introduced to get rid of the navigation code at the view and view model. It is important because otherwise view becomes to know about how it is shown on screen and that makes the reuse of the view module harder and/or requires changes inside the view module.

## Test

This folder contains only unit tests of one use case but it also should contain:

* integration tests (Check that presentation/domain/data layers are integrated correctly in one module. For example we can assemble tournament view model with use cases and test that correct data are returned to the view) 
* UI tests (Usually here we check complete flows e.g. login in -> chose tournament -> register in the tournament -> play -> check tournament results)
